package com.jornvanwier;

import com.jornvanwier.search.GeneticSearch;

import java.util.Random;

public class Main {
    public static final int N_QUEENS = 100;
    public static final int OPTIMUM = (int) ((N_QUEENS - 1) * (float) N_QUEENS / 2);

    public static void main(String[] args) {
        var random = new Random();
        final long startTime = System.nanoTime();
        new GeneticSearch(50, 1_000_000_000, 10, () -> new Board(random)).start();
        final long duration = System.nanoTime() - startTime;
        System.out.printf("Finished in %d seconds\n", duration / 1_000_000_000);
    }
}
