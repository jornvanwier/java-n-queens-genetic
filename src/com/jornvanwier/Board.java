package com.jornvanwier;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.jornvanwier.Main.N_QUEENS;
import static com.jornvanwier.Main.OPTIMUM;

public class Board {
    public List<Integer> getStorage() {
        return storage;
    }

    private List<Integer> storage;

    public Board(List<Integer> storage) {
        this.storage = storage;
    }

    Board(Random random) {
        storage = new ArrayList<>();

        init(random);
    }

    public void init(Random random) {
        storage = IntStream.range(0, N_QUEENS).boxed().collect(Collectors.toList());
        Collections.shuffle(storage);
    }

    private boolean inConflict(int columnA, int rowA, int columnB, int rowB) {
        return columnA == columnB // Same column
                || rowA == rowB // Same row
                || Math.abs(columnA - columnB) == Math.abs(rowA - rowB); // Diagonal
    }

    private boolean inConflictWithAnother(int column, int row) {
        for (int other_column = 0; other_column < N_QUEENS; other_column++) {
            var other_row = storage.get(other_column);

            // Check if same
            if (row == other_row && column == other_column) {
                continue;
            }

            if (inConflict(column, row, other_column, other_row)) {
                return true;
            }
        }

        return false;
    }

    private int countConflicts() {
        int count = 0;
        for (int queen = 0; queen < N_QUEENS; queen++) {
            for (int otherQueen = queen + 1; otherQueen < N_QUEENS; otherQueen++) {
                if (inConflict(queen, storage.get(queen), otherQueen, storage.get(otherQueen))) {
                    count++;
                }
            }
        }
        return count;
    }

    public int evaluateState() {
        return OPTIMUM - countConflicts();
    }

    public void print() {
        for (int row = 0; row < N_QUEENS; row++) {
            for (int column = 0; column < N_QUEENS; column++) {
                if (storage.get(column) == row) {
                    System.out.print(inConflictWithAnother(column, row) ? "Q " : "q ");
                }
                else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }
}
