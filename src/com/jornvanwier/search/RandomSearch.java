package com.jornvanwier.search;

import com.jornvanwier.Board;

import java.util.Random;

import static com.jornvanwier.Main.OPTIMUM;

public class RandomSearch {
    private static final int MAX = 100000;
    private final Board board;

    public RandomSearch(Board board) {
        this.board = board;
    }

    public void start() {
        var i = 0;

        var random = new Random();

        while (i < MAX && board.evaluateState() != OPTIMUM) {
            i++;
            if (i % 100 == 0) {
                System.out.printf("Iteration %d\n", i);
            }

            board.init(random);
        }

        System.out.printf("Board: %d, OPTIMUM: %d\n", board.evaluateState(), OPTIMUM);
        board.print();
    }
}
