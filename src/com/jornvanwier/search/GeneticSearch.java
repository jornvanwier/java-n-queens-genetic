package com.jornvanwier.search;

import com.jornvanwier.Board;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.jornvanwier.Main.N_QUEENS;
import static com.jornvanwier.Main.OPTIMUM;

public class GeneticSearch {
    private int populationSize;
    private int maxGeneration;
    private int nParents;
    private Supplier<Board> createFn;

    private ArrayList<Function<Board, Board>> mutationOptions;
    private Random random = new Random();
    private ArrayList<Board> population;

    public GeneticSearch(int populationSize, int maxGeneration, int nParents, Supplier<Board> createFn) {
        this.populationSize = populationSize;
        this.maxGeneration = maxGeneration;
        this.nParents = nParents;
        this.createFn = createFn;

        mutationOptions = new ArrayList<>();
        mutationOptions.add(this::mutateSwap);
        mutationOptions.add(this::mutateShift);
        mutationOptions.add(this::mutateReverseSlice);

        // Initialize population
        population = Stream.generate(createFn).limit(populationSize).collect(Collectors.toCollection(ArrayList::new));
    }

    public void start() {

        for (int generation = 0; generation < maxGeneration; generation++) {
            population.sort((boardA, boardB) -> boardB.evaluateState() - boardA.evaluateState());

            var highestFitness = population.get(0).evaluateState();

            if (generation % 100 == 0) {
                System.out.printf("Generation %d, highest fitness %d/%d\n", generation, highestFitness, OPTIMUM);
            }

            if (highestFitness == OPTIMUM) {
                System.out.printf("Solution found in %d generations\n", generation);
                population.get(0).print();
                break;
            }

            var parents = population.stream().limit(nParents).collect(Collectors.toList());
            Collections.shuffle(parents);

            var nextGeneration = new ArrayList<Board>();

            for (int i = 0; i < nParents - 1; i += 2) {
                var pair = parents.subList(i, i + 2);

                for (int j = 0; j < Math.ceil((float) populationSize / nParents * 2); j++) {
                    var child = crossover(pair);
                    var mutated = mutate(child);
                    nextGeneration.add(mutated);
                }
            }

            assert nextGeneration.size() == populationSize;

            population = nextGeneration;
        }

    }

    private Board crossover(List<Board> pair) {
        var mother = pair.get(0);
        var father = pair.get(1);

        var cut = random.nextInt(N_QUEENS);

        var result = new ArrayList<>(mother.getStorage().subList(0, cut));
        for (var queen :
                father.getStorage()) {
            if (!result.contains(queen)) {
                result.add(queen);
            }
        }

        return new Board(result);
    }

    private Board mutate(Board individual) {
        var mutateChance = 0.5;


        while (random.nextFloat() < mutateChance) {
            individual = mutationOptions.get(random.nextInt(mutationOptions.size())).apply(individual);
        }

        return individual;
    }

    private Board mutateSwap(Board individual) {
        // Swap to random numbers

        var a = random.nextInt(N_QUEENS);
        var b = random.nextInt(N_QUEENS - 1);
        if (a == b) {
            b = N_QUEENS - 1;
        }

        Collections.swap(individual.getStorage(), a, b);

        return individual;
    }

    private Board mutateShift(Board individual) {
        // Reinsert a value at a random spot

        var point = random.nextInt(N_QUEENS);
        var new_place = random.nextInt(N_QUEENS - 1);

        var storage = individual.getStorage();
        var value = storage.get(point);
        storage.remove(point);
        storage.add(new_place, value);

        return individual;
    }

    private Board mutateReverseSlice(Board individual) {
        var start = random.nextInt(N_QUEENS - 1);
        var end = random.nextInt(N_QUEENS - start - 1) + 1;

        for (int i = 0; i < (end - start) / 2; i++) {
            Collections.swap(individual.getStorage(), start + i, end - i);
        }

        return individual;
    }
}
